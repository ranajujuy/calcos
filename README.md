# calcos
Un repo con todos los calcos que nos encanta encontrar al llegar a un evento.  Las imágenes debajo son muestras, para imprimir componer una A3 o A4 utilizando los fuentes .SVG !!!

## Eventos

* Flisol Tilcara 2017
* Software Freedom Day - Salta


## Ejemplos
 
![FLISOL-logo](https://raw.githubusercontent.com/FLISOL-Argentina/calcos/master/FLISOL-logo.png)

![Soy Libre](https://raw.githubusercontent.com/FLISOL-Argentina/calcos/master/soy-libre.png)

![La Nube](https://raw.githubusercontent.com/FLISOL-Argentina/calcos/master/la-nube.png)


